﻿namespace Proyecto_1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textNombre = new System.Windows.Forms.TextBox();
            this.textHoras = new System.Windows.Forms.TextBox();
            this.textPago = new System.Windows.Forms.TextBox();
            this.textApellido = new System.Windows.Forms.TextBox();
            this.textHorasExtra = new System.Windows.Forms.TextBox();
            this.textPagoExtra = new System.Windows.Forms.TextBox();
            this.buttonTotal = new System.Windows.Forms.Button();
            this.labelResPago = new System.Windows.Forms.Label();
            this.labelResExtra = new System.Windows.Forms.Label();
            this.labelResTotal = new System.Windows.Forms.Label();
            this.button_binary = new System.Windows.Forms.Button();
            this.button_ReadBin = new System.Windows.Forms.Button();
            this.buttonClear = new System.Windows.Forms.Button();
            this.buttonShowAll = new System.Windows.Forms.Button();
            this.textBoxVeces = new System.Windows.Forms.TextBox();
            this.labelContador = new System.Windows.Forms.Label();
            this.buttonCalcular = new System.Windows.Forms.Button();
            this.buttonPromedio = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(26, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Nombre";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(221, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Horas Trab";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(213, 73);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(68, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Pagos Horas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(26, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(44, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Apellido";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(194, 114);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Horas Extra Trab";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(191, 140);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(90, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Pago Horas Extra";
            // 
            // textNombre
            // 
            this.textNombre.Location = new System.Drawing.Point(78, 47);
            this.textNombre.Name = "textNombre";
            this.textNombre.Size = new System.Drawing.Size(100, 20);
            this.textNombre.TabIndex = 6;
            this.textNombre.TextChanged += new System.EventHandler(this.textNombre_TextChanged);
            // 
            // textHoras
            // 
            this.textHoras.Location = new System.Drawing.Point(287, 43);
            this.textHoras.Name = "textHoras";
            this.textHoras.Size = new System.Drawing.Size(100, 20);
            this.textHoras.TabIndex = 7;
            this.textHoras.TextChanged += new System.EventHandler(this.textHoras_TextChanged);
            // 
            // textPago
            // 
            this.textPago.Location = new System.Drawing.Point(287, 66);
            this.textPago.Name = "textPago";
            this.textPago.Size = new System.Drawing.Size(100, 20);
            this.textPago.TabIndex = 8;
            this.textPago.TextChanged += new System.EventHandler(this.textPago_TextChanged);
            // 
            // textApellido
            // 
            this.textApellido.Location = new System.Drawing.Point(78, 71);
            this.textApellido.Name = "textApellido";
            this.textApellido.Size = new System.Drawing.Size(100, 20);
            this.textApellido.TabIndex = 9;
            this.textApellido.TextChanged += new System.EventHandler(this.textApellido_TextChanged);
            // 
            // textHorasExtra
            // 
            this.textHorasExtra.Location = new System.Drawing.Point(287, 107);
            this.textHorasExtra.Name = "textHorasExtra";
            this.textHorasExtra.Size = new System.Drawing.Size(100, 20);
            this.textHorasExtra.TabIndex = 10;
            this.textHorasExtra.TextChanged += new System.EventHandler(this.textHorasExtra_TextChanged);
            // 
            // textPagoExtra
            // 
            this.textPagoExtra.Location = new System.Drawing.Point(287, 133);
            this.textPagoExtra.Name = "textPagoExtra";
            this.textPagoExtra.Size = new System.Drawing.Size(100, 20);
            this.textPagoExtra.TabIndex = 11;
            this.textPagoExtra.TextChanged += new System.EventHandler(this.textPagoExtra_TextChanged);
            // 
            // buttonTotal
            // 
            this.buttonTotal.Location = new System.Drawing.Point(67, 242);
            this.buttonTotal.Name = "buttonTotal";
            this.buttonTotal.Size = new System.Drawing.Size(331, 30);
            this.buttonTotal.TabIndex = 14;
            this.buttonTotal.Text = "Almacenar y Continuar";
            this.buttonTotal.UseVisualStyleBackColor = true;
            this.buttonTotal.Click += new System.EventHandler(this.button3_Click);
            // 
            // labelResPago
            // 
            this.labelResPago.AutoSize = true;
            this.labelResPago.Location = new System.Drawing.Point(63, 169);
            this.labelResPago.Name = "labelResPago";
            this.labelResPago.Size = new System.Drawing.Size(10, 13);
            this.labelResPago.TabIndex = 15;
            this.labelResPago.Text = "-";
            // 
            // labelResExtra
            // 
            this.labelResExtra.AutoSize = true;
            this.labelResExtra.Location = new System.Drawing.Point(134, 169);
            this.labelResExtra.Name = "labelResExtra";
            this.labelResExtra.Size = new System.Drawing.Size(10, 13);
            this.labelResExtra.TabIndex = 16;
            this.labelResExtra.Text = "-";
            // 
            // labelResTotal
            // 
            this.labelResTotal.AutoSize = true;
            this.labelResTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelResTotal.ForeColor = System.Drawing.Color.Lime;
            this.labelResTotal.Location = new System.Drawing.Point(191, 153);
            this.labelResTotal.Name = "labelResTotal";
            this.labelResTotal.Size = new System.Drawing.Size(26, 33);
            this.labelResTotal.TabIndex = 17;
            this.labelResTotal.Text = "-";
            // 
            // button_binary
            // 
            this.button_binary.Enabled = false;
            this.button_binary.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.button_binary.Location = new System.Drawing.Point(417, 14);
            this.button_binary.Name = "button_binary";
            this.button_binary.Size = new System.Drawing.Size(66, 20);
            this.button_binary.TabIndex = 19;
            this.button_binary.Text = "Crear Bin";
            this.button_binary.UseVisualStyleBackColor = true;
            this.button_binary.Click += new System.EventHandler(this.button_binary_Click);
            // 
            // button_ReadBin
            // 
            this.button_ReadBin.Enabled = false;
            this.button_ReadBin.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.button_ReadBin.Location = new System.Drawing.Point(408, 40);
            this.button_ReadBin.Name = "button_ReadBin";
            this.button_ReadBin.Size = new System.Drawing.Size(75, 20);
            this.button_ReadBin.TabIndex = 20;
            this.button_ReadBin.Text = "Lee Binario";
            this.button_ReadBin.UseVisualStyleBackColor = true;
            this.button_ReadBin.Click += new System.EventHandler(this.button_ReadBin_Click);
            // 
            // buttonClear
            // 
            this.buttonClear.Location = new System.Drawing.Point(224, 11);
            this.buttonClear.Name = "buttonClear";
            this.buttonClear.Size = new System.Drawing.Size(100, 23);
            this.buttonClear.TabIndex = 21;
            this.buttonClear.Text = "Reiniciar";
            this.buttonClear.UseVisualStyleBackColor = true;
            this.buttonClear.Click += new System.EventHandler(this.buttonClear_Click);
            // 
            // buttonShowAll
            // 
            this.buttonShowAll.Enabled = false;
            this.buttonShowAll.Location = new System.Drawing.Point(298, 200);
            this.buttonShowAll.Name = "buttonShowAll";
            this.buttonShowAll.Size = new System.Drawing.Size(100, 36);
            this.buttonShowAll.TabIndex = 22;
            this.buttonShowAll.Text = "Mostrar Todo";
            this.buttonShowAll.UseVisualStyleBackColor = true;
            this.buttonShowAll.Click += new System.EventHandler(this.buttonShowAll_Click);
            // 
            // textBoxVeces
            // 
            this.textBoxVeces.Location = new System.Drawing.Point(78, 21);
            this.textBoxVeces.Name = "textBoxVeces";
            this.textBoxVeces.Size = new System.Drawing.Size(27, 20);
            this.textBoxVeces.TabIndex = 25;
            // 
            // labelContador
            // 
            this.labelContador.AutoSize = true;
            this.labelContador.Location = new System.Drawing.Point(111, 24);
            this.labelContador.Name = "labelContador";
            this.labelContador.Size = new System.Drawing.Size(10, 13);
            this.labelContador.TabIndex = 26;
            this.labelContador.Text = "-";
            // 
            // buttonCalcular
            // 
            this.buttonCalcular.Location = new System.Drawing.Point(67, 200);
            this.buttonCalcular.Name = "buttonCalcular";
            this.buttonCalcular.Size = new System.Drawing.Size(225, 36);
            this.buttonCalcular.TabIndex = 27;
            this.buttonCalcular.Text = "Calcular Todo";
            this.buttonCalcular.UseVisualStyleBackColor = true;
            this.buttonCalcular.Click += new System.EventHandler(this.buttonCalcular_Click);
            // 
            // buttonPromedio
            // 
            this.buttonPromedio.Enabled = false;
            this.buttonPromedio.Location = new System.Drawing.Point(404, 200);
            this.buttonPromedio.Name = "buttonPromedio";
            this.buttonPromedio.Size = new System.Drawing.Size(79, 72);
            this.buttonPromedio.TabIndex = 28;
            this.buttonPromedio.Text = "Promedio";
            this.buttonPromedio.UseVisualStyleBackColor = true;
            this.buttonPromedio.Click += new System.EventHandler(this.button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 291);
            this.Controls.Add(this.buttonPromedio);
            this.Controls.Add(this.buttonCalcular);
            this.Controls.Add(this.labelContador);
            this.Controls.Add(this.textBoxVeces);
            this.Controls.Add(this.buttonShowAll);
            this.Controls.Add(this.buttonClear);
            this.Controls.Add(this.button_ReadBin);
            this.Controls.Add(this.button_binary);
            this.Controls.Add(this.labelResTotal);
            this.Controls.Add(this.labelResExtra);
            this.Controls.Add(this.labelResPago);
            this.Controls.Add(this.buttonTotal);
            this.Controls.Add(this.textPagoExtra);
            this.Controls.Add(this.textHorasExtra);
            this.Controls.Add(this.textApellido);
            this.Controls.Add(this.textPago);
            this.Controls.Add(this.textHoras);
            this.Controls.Add(this.textNombre);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textNombre;
        private System.Windows.Forms.TextBox textHoras;
        private System.Windows.Forms.TextBox textPago;
        private System.Windows.Forms.TextBox textApellido;
        private System.Windows.Forms.TextBox textHorasExtra;
        private System.Windows.Forms.TextBox textPagoExtra;
        private System.Windows.Forms.Button buttonTotal;
        private System.Windows.Forms.Label labelResPago;
        private System.Windows.Forms.Label labelResExtra;
        private System.Windows.Forms.Label labelResTotal;
        private System.Windows.Forms.Button button_binary;
        private System.Windows.Forms.Button button_ReadBin;
        private System.Windows.Forms.Button buttonClear;
        private System.Windows.Forms.Button buttonShowAll;
        private System.Windows.Forms.TextBox textBoxVeces;
        private System.Windows.Forms.Label labelContador;
        private System.Windows.Forms.Button buttonCalcular;
        private System.Windows.Forms.Button buttonPromedio;
    }
}

