﻿using System;
using System.Windows.Forms;
using System.IO;

namespace Proyecto_1
{
    public partial class Form1 : Form
    {
        //ClaseBase.ClaseBase obj = new ClaseBase.ClaseBase();
        ClaseDerivada.ClaseDerivada obj_2 = new ClaseDerivada.ClaseDerivada();
        

        Int16[] resultadoNormal;
        Int16[] resultadoExtra;
        Int16[] resultadoTotal;

        Int16 contador = 0;
        Boolean firstTime = true;

        public Form1()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            //Calculates
            labelResPago.Text = Convert.ToString(obj_2.P_normal());
            labelResExtra.Text = Convert.ToString(obj_2.P_extra());
            labelResTotal.Text = Convert.ToString(obj_2.P_total());

            if (textBoxVeces.Text == "")
            {
                textBoxVeces.Text = "1";
            }

            if (firstTime)
            {
                firstTime = false;
                resultadoNormal = new Int16[Convert.ToInt16(textBoxVeces.Text)];
                resultadoExtra = new Int16[Convert.ToInt16(textBoxVeces.Text)];
                resultadoTotal = new Int16[Convert.ToInt16(textBoxVeces.Text)];

                textNombre.ReadOnly = true;
                textApellido.ReadOnly = true;
            }

            if (labelResPago.Text != "-" && labelResExtra.Text != "-" && labelResTotal.Text != "-")
            {
                textPago.Text = "";
                textHoras.Text = "";
                textPagoExtra.Text = "";
                textHorasExtra.Text = "";

                //Stores on global variable
                resultadoNormal[contador] = Convert.ToInt16(labelResPago.Text);
                resultadoExtra[contador] = Convert.ToInt16(labelResExtra.Text);
                resultadoTotal[contador] = Convert.ToInt16(labelResTotal.Text);
                contador++;
                labelContador.Text = Convert.ToString(contador);

                
                if (contador >= Convert.ToInt16(textBoxVeces.Text))
                {
                    buttonTotal.Enabled = false;
                    textNombre.ReadOnly = true;
                    textPago.ReadOnly = true;
                    textHoras.ReadOnly = true;

                    textApellido.ReadOnly = true;
                    textPagoExtra.ReadOnly = true;
                    textHorasExtra.ReadOnly = true;

                    button_binary.Enabled = true;

                    // Once it ends, store info on txt
                    StreamWriter regis;
                    regis = new StreamWriter(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Datos.txt", false);
                    for (Int16 i = 0; i < contador; i++)
                    {
                        regis.WriteLine(resultadoNormal[i]);
                        regis.WriteLine(resultadoExtra[i]);
                        regis.WriteLine(resultadoTotal[i]);
                    }
                    regis.Close();
                    buttonShowAll.Enabled = true;
                    buttonPromedio.Enabled = true;
                }
            }
        }

        private void textNombre_TextChanged(object sender, EventArgs e)
        {
            obj_2.PNombre = textNombre.Text;
        }

        private void textHoras_TextChanged(object sender, EventArgs e)
        {
            if (textHoras.Text != "")
            {
                obj_2.P_hrTrab = Convert.ToInt16(textHoras.Text);
            }
        }

        private void textPago_TextChanged(object sender, EventArgs e)
        {
            if (textPago.Text != "")
            {
                obj_2.P_pagoHr = Convert.ToInt16(textPago.Text);
            }    
        }

        private void textApellido_TextChanged(object sender, EventArgs e)
        {
            obj_2.P_apellido = textApellido.Text;
        }

        private void textHorasExtra_TextChanged(object sender, EventArgs e)
        {
            if (textHorasExtra.Text != "")
            {
                obj_2.P_hrTrabEx = Convert.ToInt16(textHorasExtra.Text);
            }
        }

        private void textPagoExtra_TextChanged(object sender, EventArgs e)
        {
            if (textPagoExtra.Text != "")
            {
                obj_2.P_pagoHrEx = Convert.ToInt16(textPagoExtra.Text);
            }
        }

        private void button_binary_Click(object sender, EventArgs e)
        {
            Stream regbin;
            regbin = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Datos.bin", FileMode.Create, FileAccess.Write);
            BinaryWriter NIA = new BinaryWriter(regbin);
            for (Int16 i = 0; i < contador; i++)
            {
                NIA.Write(resultadoNormal[i]);
                NIA.Write(resultadoExtra[i]);
                NIA.Write(resultadoTotal[i]);
            }
            NIA.Close();

            button_binary.Enabled = false;
            button_ReadBin.Enabled = true;
            //Para verificar el final de un archivo binario de usa
            //PeekChar()<>-1
        }

        private void button_ReadBin_Click(object sender, EventArgs e)
        {
            Stream regbin;
            regbin = File.Open(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Datos.bin", FileMode.Open, FileAccess.Read);
            BinaryReader NIA = new BinaryReader(regbin);

            labelContador.Text = Convert.ToString(contador);
            if (textBoxVeces.Text == "")
            {
                textBoxVeces.Text = "1";
            }
            for (Int16 loop = 0; loop < contador; loop++)
            {
                labelResPago.Text = Convert.ToString(NIA.ReadInt16());
                labelResExtra.Text = Convert.ToString(NIA.ReadInt16());
                labelResTotal.Text = Convert.ToString(NIA.ReadInt16());
                labelResPago.Refresh();
                labelResExtra.Refresh();
                labelResTotal.Refresh();
                System.Threading.Thread.Sleep(1500);
            }
            NIA.Close();
        }

        private void buttonClear_Click(object sender, EventArgs e)
        {
            //File.Delete(@"C:/Users/luisa/Desktop/Datos.txt");

            contador = 0;
            labelContador.Text = Convert.ToString(contador);
            firstTime = true;

            textNombre.Text = "";
            textPago.Text = "";
            textHoras.Text = "";
            textApellido.Text = "";
            textPagoExtra.Text = "";
            textHorasExtra.Text = "";

            labelResPago.Text = "-";
            labelResExtra.Text = "-";
            labelResTotal.Text = "-";

            buttonTotal.Enabled = true;

            textNombre.ReadOnly = false;
            textPago.ReadOnly = false;
            textHoras.ReadOnly = false;

            textApellido.ReadOnly = false;
            textPagoExtra.ReadOnly = false;
            textHorasExtra.ReadOnly = false;


            buttonShowAll.Enabled = false;
            buttonPromedio.Enabled = false;

            button_binary.Enabled = false;
            button_ReadBin.Enabled = false;
        }
        
        private void buttonShowAll_Click(object sender, EventArgs e)
        {
            StreamReader regdos;
            regdos = new StreamReader(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "/Datos.txt" );

            labelContador.Text = Convert.ToString(contador);
            if (textBoxVeces.Text == "")
            {
                textBoxVeces.Text = "1";
            }
            for (Int16 loop = 0; loop < contador; loop++)
            {
                labelResPago.Text = regdos.ReadLine();
                labelResExtra.Text = regdos.ReadLine();
                labelResTotal.Text = regdos.ReadLine();
                labelResPago.Refresh();
                labelResExtra.Refresh();
                labelResTotal.Refresh();
                System.Threading.Thread.Sleep(1500);
            }
            regdos.Close();
        }

        private void buttonCalcular_Click(object sender, EventArgs e)
        {
            labelResPago.Text = Convert.ToString(obj_2.P_normal());
            labelResExtra.Text = Convert.ToString(obj_2.P_extra());
            labelResTotal.Text = Convert.ToString(obj_2.P_total());

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Promedios.ventanaPromedios promedios = new Promedios.ventanaPromedios();
            //Sending three arrays to the other windows
            promedios.setSizes(resultadoNormal, resultadoExtra, resultadoTotal);
            promedios.Show();
        }
    }
}
