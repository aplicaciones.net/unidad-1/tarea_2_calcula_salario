﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaseBase
{
    public class ClaseBase
    {
        //Atributos de la clase Proyecto_1
        private String nombre;

        //PRopiedad para nombre
        public String PNombre
        {
            get
            {
                return nombre;
            }
            set
            {
                nombre = value;
            }
        }


        private Int16 pagoHr;
        public Int16 P_pagoHr
        {
            get
            {
                return pagoHr;
            }
            set
            {
                pagoHr = value;
            }
        }
        private Int16 hrTrab;
        public Int16 P_hrTrab
        {
            get
            {
                return hrTrab;
            }
            set
            {
                hrTrab = value;
            }
        }

    }
}
