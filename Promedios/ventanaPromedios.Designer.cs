﻿namespace Promedios
{
    partial class ventanaPromedios
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelPromedioNormal = new System.Windows.Forms.Label();
            this.labelPromedioExtra = new System.Windows.Forms.Label();
            this.labelPromedioTotal = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelPromedioNormal
            // 
            this.labelPromedioNormal.AutoSize = true;
            this.labelPromedioNormal.Location = new System.Drawing.Point(66, 51);
            this.labelPromedioNormal.Name = "labelPromedioNormal";
            this.labelPromedioNormal.Size = new System.Drawing.Size(10, 13);
            this.labelPromedioNormal.TabIndex = 0;
            this.labelPromedioNormal.Text = "-";
            // 
            // labelPromedioExtra
            // 
            this.labelPromedioExtra.AutoSize = true;
            this.labelPromedioExtra.Location = new System.Drawing.Point(202, 51);
            this.labelPromedioExtra.Name = "labelPromedioExtra";
            this.labelPromedioExtra.Size = new System.Drawing.Size(10, 13);
            this.labelPromedioExtra.TabIndex = 1;
            this.labelPromedioExtra.Text = "-";
            // 
            // labelPromedioTotal
            // 
            this.labelPromedioTotal.AutoSize = true;
            this.labelPromedioTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelPromedioTotal.ForeColor = System.Drawing.Color.Lime;
            this.labelPromedioTotal.Location = new System.Drawing.Point(123, 103);
            this.labelPromedioTotal.Name = "labelPromedioTotal";
            this.labelPromedioTotal.Size = new System.Drawing.Size(26, 33);
            this.labelPromedioTotal.TabIndex = 2;
            this.labelPromedioTotal.Text = "-";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.labelPromedioTotal);
            this.Controls.Add(this.labelPromedioExtra);
            this.Controls.Add(this.labelPromedioNormal);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelPromedioNormal;
        private System.Windows.Forms.Label labelPromedioExtra;
        private System.Windows.Forms.Label labelPromedioTotal;
    }
}

