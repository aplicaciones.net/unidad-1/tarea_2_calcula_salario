﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Promedios
{
    public partial class ventanaPromedios : Form
    {
        public Int16[] normal;
        public Int16[] extra;
        public Int16[] total;


        public ventanaPromedios()
        {
            InitializeComponent();
        }

        public void setSizes(Int16[] normal, Int16[] extra, Int16[] total)
        {
            this.normal = new Int16[normal.Length];
            this.extra = new Int16[extra.Length];
            this.total = new Int16[total.Length];

            this.normal = normal;
            this.extra = extra;
            this.total = total;

            promedios(labelPromedioNormal, normal);
            promedios(labelPromedioExtra, extra);
            promedios(labelPromedioTotal, total);
        }

        private void promedios(Label thisLabel, Int16[] thisArray)
        {
            Int16 res = 0;
            for (Int16 count = 0; count < thisArray.Length; count ++)
            {
                res += thisArray[count];
            }
            thisLabel.Text = Convert.ToString(res/ thisArray.Length);
        }
    }
}
