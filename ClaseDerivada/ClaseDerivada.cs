﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClaseDerivada
{
    public class ClaseDerivada : ClaseBase.ClaseBase
    {

        private String apellido;

        //PRopiedad para nombre
        public String P_apellido
        {
            get
            {
                return apellido;
            }
            set
            {
                apellido = value;
            }
        }


        private Int16 pagoHrEx;
        public Int16 P_pagoHrEx
        {
            get
            {
                return pagoHrEx;
            }
            set
            {
                pagoHrEx = value;
            }
        }
        private Int16 hrTrabEx;
        public Int16 P_hrTrabEx
        {
            get
            {
                return hrTrabEx;
            }
            set
            {
                hrTrabEx = value;
            }
        }
        public Decimal P_extra()
        {
            Decimal respE;
            respE = P_pagoHrEx * P_hrTrabEx;
            return (respE);
        }
        public Decimal P_normal()
        {
            Decimal respN;
            respN = P_pagoHr * P_hrTrab;
            return (respN);
        }


        public Decimal P_total()
        {
            Decimal resp;
            resp = (P_pagoHr * P_hrTrab) + (P_pagoHrEx * P_hrTrabEx);
            return (resp);
        }



    }
}
